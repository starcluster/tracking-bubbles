"""
This code has the purpose to track runaways bubbles in a sink, we use trackpy, numpy, pims, pandas
and numba for optmization purposes.
This script is also able to compute the average distance before the bubles dies.
Written by Pierre Wulles for the international contest of physics.
January 2019 
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc('figure',  figsize=(10, 6))
mpl.rc('image', cmap='gray')

import numpy as np
import pandas as pd
from pandas import DataFrame, Series

import pims
import trackpy as tp

frames = pims.ImageSequence('./RENDER1_HD/*.png', as_grey=True) #Load the frames

plt.imshow(frames[0])
f = tp.locate(frames[0],diameter= 59,minmass=10000, invert=False)
#play with the value of minmass to have the best spotting
plt.figure()
tp.annotate(f, frames[0])
plt.show()
f = tp.batch(frames[:], 59, minmass=10000, invert=False, engine='numba')
#I use numba for optimization, remove if you don't have it installed
t = tp.link_df(f, 50, memory=3)
t1 = tp.filter_stubs(t, 10)
particles = {}
for i in t1.values:
    x = i[1]
    y = i[0]
    no_particle = i[len(i) - 1]
    particles[no_particle] = (x,y)
print(particles)
print(t1.groupby('particle').agg(tp.diagonal_size))
origin = [1850,510]# What is according to you the origin of 
#origin = [950,0] 
distances = []

#Compute the distance from the origin to the end of the trajectory
for i in particles.values():
    x1,y1 = [origin[0],i[0]],[origin[1],i[1]]
    y1 = [i[0],i[1]]
    x_max = i[0]
    y_max = i[1]
    d = np.sqrt((origin[0] - x_max)**2 + (origin[1] - y_max)**2)
    distances.append(d)

bins = [ i*60 for i in range(16)]
plt.hist(distances,bins)
plt.show()
tp.plot_traj(t1)
print(sum(distances)/len(distances))
